﻿using ExApiConnectDB.EntityDB;
using ExApiConnectDB.Interface;
using ExApiConnectDB.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ExApiConnectDB
{
    public class AuthenticationHandler : DelegatingHandler
    {

        private IAccessTokenService accessTokenService;
        public AuthenticationHandler()
        {
            this.accessTokenService = new DBAccessTokenService();
        }
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var Auth = request.Headers.Authorization;
            if (Auth != null)
             {
                string AccessToken = Auth.Parameter;
                string AccessTokenType = Auth.Scheme;

                if(AccessTokenType.Equals("Bearer"))
                {
                    var UserLoginVerify = this.accessTokenService.VerifyAccessToken(AccessToken);
                    if (UserLoginVerify != null)
                    {
                        //var userLogin = new UserLogin(new GenericIdentity(UserLoginVerify.email, UserLoginVerify.role));
                        var userLogin = new UserLogin(new GenericIdentity(UserLoginVerify.email), UserLoginVerify.role);
                        userLogin.Member = UserLoginVerify;
                        Thread.CurrentPrincipal = userLogin;
                        HttpContext.Current.User = userLogin;
                    }

                }
            }
            return base.SendAsync(request, cancellationToken);
        }
    }

    public class UserLogin : GenericPrincipal
    {

        public Member Member { get; set; }
        public UserLogin(IIdentity identity, RoleAccount roles) : base(identity, new string[] { roles.ToString() })
        {

        }
    }
}