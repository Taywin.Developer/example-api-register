﻿using ExApiConnectDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExApiConnectDB.Interface
{
    interface IAccountService
    {
        void Register(RegisterModels model);
        bool Login(LoginModels model);
    }
}
