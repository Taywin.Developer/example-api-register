﻿using ExApiConnectDB.EntityDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExApiConnectDB.Interface
{
    interface IAccessTokenService
    {
        string GenerateAccessToken(string email, int minute = 60);
        Member VerifyAccessToken(string AccessToken);
    }
}
