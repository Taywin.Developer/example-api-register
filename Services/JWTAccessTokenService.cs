﻿using ExApiConnectDB.EntityDB;
using ExApiConnectDB.Interface;
using Jose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ExApiConnectDB.Services
{
    public class JWTAccessTokenService : IAccessTokenService
    {
        private byte[] secretKey = Encoding.UTF8.GetBytes("C# ASP.NET MEMBER WORKSHOP");

        public string GenerateAccessToken(string email, int minute = 60)
        {
            JwtPayload payload = new JwtPayload
            {
                email = email,
                exp = DateTime.UtcNow.AddMinutes(minute)
            };
            return JWT.Encode(payload, this.secretKey, JwsAlgorithm.HS256);
        }

        public Member VerifyAccessToken(string AccessToken)
        {
            throw new NotImplementedException();
        }
    }

    public class JwtPayload
    {
        public string email { get; set; }
        public DateTime exp { get; set; }
    }
}