﻿using ExApiConnectDB.EntityDB;
using ExApiConnectDB.Interface;
using ExApiConnectDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExApiConnectDB.Services
{
    public class AccountService : IAccountService
    {
        private testEntities db = new testEntities();

        public bool Login(LoginModels model)
        {
            try
            {
                var memberItem = this.db.Members.SingleOrDefault(m => m.email.Equals(model.email));
                if (memberItem != null)
                {
                    return PasswordHashModel.Verify(model.password, memberItem.password);
                }

                return false;
            }
            catch (Exception e)
            {
                throw e.GetErrorException();
            }
        }

        public void Register(RegisterModels model)
        {
            //throw new NotImplementedException();
            try
            {
                this.db.Members.Add(new Member
                {
                    firstname = model.firstname,
                    lastname = model.lastname,
                    email = model.email,
                    password = model.password,
                    position = "",
                    image = null,
                    role = RoleAccount.Member,
                    created = DateTime.Now,
                    updated = DateTime.Now
                });

                this.db.SaveChanges();
            }
            catch(Exception e)
            {
                throw e.GetErrorException();
            }
        }
    }
}