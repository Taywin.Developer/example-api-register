﻿using ExApiConnectDB.EntityDB;
using ExApiConnectDB.Interface;
using ExApiConnectDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExApiConnectDB.Services
{
    public class DBAccessTokenService : IAccessTokenService
    {
        private testEntities db = new testEntities();
        public string GenerateAccessToken(string email, int minute = 60)
        {
           try
            {
                var memberItem = this.db.Members.SingleOrDefault(m => m.email.Equals(email));
                if (memberItem == null) throw new Exception("not found");
                var accessTokenCreate = new AccessToken
                {
                    token = Guid.NewGuid().ToString(),
                    exprise = DateTime.Now.AddMinutes(minute),
                    memberID = memberItem.id
                };
                this.db.AccessTokens.Add(accessTokenCreate);
                this.db.SaveChanges();
                return accessTokenCreate.token;
            }
            catch(Exception e)
            {
                throw e.GetErrorException();
            }
        }

        public Member VerifyAccessToken(string accessToken)
        {
           try
            {
                var accessTokenItem = this.db.AccessTokens.SingleOrDefault(item => item.token.Equals(accessToken));
                if (accessTokenItem == null) return null;
                if (accessTokenItem.exprise < DateTime.Now) return null;

                return accessTokenItem.Member;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}