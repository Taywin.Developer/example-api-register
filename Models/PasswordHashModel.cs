﻿using SimplePassword;

namespace ExApiConnectDB.Models
{
    public class PasswordHashModel
    {
        public static string Hash(string password)
        {
            var saltedPasswordHash = new SaltedPasswordHash(password, 20);
            return saltedPasswordHash.Hash + ":" + saltedPasswordHash.Salt;
        }

        //verify password method
        public static bool Verify(string password, string passwordHash)
        {
            string[] passwordHashes = passwordHash.Split(':');
            if (passwordHashes.Length == 2)
            {
                var saltedPasswordHash = new SaltedPasswordHash(passwordHashes[0], passwordHashes[1]);
                return saltedPasswordHash.Verify(password);
            }
            return false;
        }

    }
}