﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExApiConnectDB.Models
{
    public class LoginModels
    {
        [Required]
        public string email { get; set; }

        [Required]
        public string password { get; set; }

        public bool remember { get; set; }

    }

}