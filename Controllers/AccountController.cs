﻿using ExApiConnectDB.Interface;
using ExApiConnectDB.Models;
using ExApiConnectDB.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExApiConnectDB.Controllers
{
    public class AccountController : ApiController
    {
        private IAccountService Account;
        private IAccessTokenService AccessToken;

        protected AccountController()
        {
            this.Account = new AccountService();
            this.AccessToken = new DBAccessTokenService();
        }

        //การลงทะเบียน
        [Route("api/account/register")]
        public IHttpActionResult PostRegister([FromBody] RegisterModels model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    model.password = PasswordHashModel.Hash(model.password);
                    this.Account.Register(model);
                    return Ok("Successful");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Exception", ex.Message);
                }
            }
            return BadRequest(ModelState.GetErrorModelState());
        }

        //การเข้าสู่ระบบ
        [Route("api/account/login")]
        public AccessTokenModel PostLogin([FromBody] LoginModels model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (this.Account.Login(model))
                    {
                        return new AccessTokenModel
                        {
                            accessToken = this.AccessToken.GenerateAccessToken(model.email)
                        };
                    }
                    throw new Exception("Username or Password is invalid");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Exception", e.Message);
                }
            }
            throw new HttpResponseException(Request.CreateResponse(
               HttpStatusCode.BadRequest,
               new { Message = ModelState.GetErrorModelState() }
           ));
        }
    }
}
