﻿using ExApiConnectDB.EntityDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ExApiConnectDB.Controllers
{
    [Authorize]
    public class MemberController : ApiController
    {

        [Route("api/member/data")]
        public Member GetMemberLogin()
        {
            var userLogin = User as UserLogin;
            return new Member
            {
                id = userLogin.Member.id,
                created = userLogin.Member.created,
                email = userLogin.Member.email,
                firstname = userLogin.Member.firstname,
                lastname = userLogin.Member.lastname,
                image = userLogin.Member.image,
                position = userLogin.Member.position,
                role = userLogin.Member.role,
                updated = userLogin.Member.updated
            };
        }
    }
}
